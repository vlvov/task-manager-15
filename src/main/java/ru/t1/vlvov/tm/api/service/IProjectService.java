package ru.t1.vlvov.tm.api.service;

import ru.t1.vlvov.tm.enumerated.Sort;
import ru.t1.vlvov.tm.enumerated.Status;
import ru.t1.vlvov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService {

    Project add(Project project);

    Project changeProjectStatusById(String id, Status status);

    Project changeProjectStatusByIndex(Integer index, Status status);

    Project create(String name);

    Project create(String name, String description);

    void clear();

    List<Project> findAll();

    List<Project> findAll(Comparator comparator);

    List<Project> findAll(Sort sort);

    void remove(Project project);

    Project removeById(String id);

    Project removeByIndex(Integer index);

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

}
